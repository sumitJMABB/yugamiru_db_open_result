﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
   
    public class IntegerRangeDefinitionElement
    {
        public string m_strRangeSymbol;       // ’lˆæƒVƒ“ƒ{ƒ‹.
        public int m_iMinValueOperatorID;  // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
        public int m_iMaxValueOperatorID;  // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
        public int m_iMinValue;            // Å¬’l.
        public int m_iMaxValue;

        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // ®”’lˆæƒVƒ“ƒ{ƒ‹’è‹`‚Ìƒ}ƒXƒ^[ƒf[ƒ^‚ÌƒXƒL[ƒ}.
        public IntegerRangeDefinitionElement()
        {
            m_strRangeSymbol = string.Empty;						// ’lˆæƒVƒ“ƒ{ƒ‹.

    m_iMinValueOperatorID = Constants.COMPAREOPERATORID_NONE;   // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMaxValueOperatorID = Constants.COMPAREOPERATORID_NONE;   // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.

            m_iMinValue = 0;                                    // Å¬’l.

            m_iMaxValue = 0;                                 // Å‘å’l.
        
        }

       

    public int ReadFromString(ref char[] lpszText)
    {
        string strRangeSymbol = string.Empty;
            string strMinValueOperator = string.Empty;
            string strMinValue = string.Empty;
            string strMaxValueOperator = string.Empty;
            string strMaxValue = string.Empty;
            string strMinusInfinity = string.Empty;
            string strPlusInfinity = string.Empty;

            TextBuffer TextBuffer = new TextBuffer(lpszText);

        TextBuffer.SkipSpaceOrTab();
        if (TextBuffer.IsEOF())
        {
            return (Constants.READFROMSTRING_NULLLINE);
        }

        if (TextBuffer.ReadNewLine())
        {
            if (TextBuffer.IsEOF())
            {
                return (Constants.READFROMSTRING_NULLLINE);
            }
        }

        if (!TextBuffer.ReadSymbol(ref strRangeSymbol))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strMinValueOperator))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSignedValue(ref strMinValue))
        {
            strMinValue = "";
            if (!TextBuffer.ReadSymbol(ref strMinusInfinity))
            {
                return (Constants.READFROMSTRING_SYNTAX_ERROR);
            }
            if (strMinusInfinity != "MINUS_INFINITY")
            {
                return (Constants.READFROMSTRING_SYNTAX_ERROR);
            }
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strMaxValueOperator))
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSignedValue(ref strMaxValue))
        {
                strMaxValue = "";
            if (!TextBuffer.ReadSymbol(ref strPlusInfinity))
            {
                return (Constants.READFROMSTRING_SYNTAX_ERROR);
            }
            if (strPlusInfinity != "PLUS_INFINITY")
            {
                return (Constants.READFROMSTRING_SYNTAX_ERROR);
            }
        }

        TextBuffer.SkipSpaceOrTab();
        TextBuffer.ReadNewLine();
        if (!TextBuffer.IsEOF())
        {
            return (Constants.READFROMSTRING_SYNTAX_ERROR);
        }

        m_strRangeSymbol = strRangeSymbol;
        m_iMinValueOperatorID = m_SymbolFunc.GetCompareOperatorIDBySymbol(strMinValueOperator);
        if ((m_iMinValueOperatorID != Constants.COMPAREOPERATORID_GREATEROREQUAL) &&
            (m_iMinValueOperatorID != Constants.COMPAREOPERATORID_GREATER))
        {
            return (Constants.READFROMSTRING_MINVALUE_COMPAREOPERATOR_INVALID);
        }
        m_iMaxValueOperatorID = m_SymbolFunc.GetCompareOperatorIDBySymbol(strMaxValueOperator);
        if ((m_iMaxValueOperatorID != Constants.COMPAREOPERATORID_LESSOREQUAL) &&
            (m_iMaxValueOperatorID != Constants.COMPAREOPERATORID_LESS))
        {
            return (Constants.READFROMSTRING_MAXVALUE_COMPAREOPERATOR_INVALID);
        }

        if (strMinusInfinity == "MINUS_INFINITY")
        {
            m_iMinValueOperatorID = Constants.COMPAREOPERATORID_TRUE;
            m_iMinValue = 0;
        }
        else
        {
            m_iMinValue =Int32.Parse(strMinValue);
        }

        if (strPlusInfinity == "PLUS_INFINITY")
        {
            m_iMaxValueOperatorID = Constants.COMPAREOPERATORID_TRUE;
            m_iMaxValue = 0;
        }
        else
        {
            m_iMaxValue = Int32.Parse(strMaxValue);
        }
        if ((m_iMinValueOperatorID != Constants.COMPAREOPERATORID_TRUE) && (m_iMaxValueOperatorID != Constants.COMPAREOPERATORID_TRUE))
        {
            if (m_iMinValue > m_iMaxValue)
            {
                return Constants.READFROMSTRING_RANGE_INVALID;
            }
        }

        return Constants.READFROMSTRING_SYNTAX_OK;
    }

    public bool IsEqualRangeSymbol( string pchRangeSymbol) 
{
	return( m_strRangeSymbol == pchRangeSymbol );
}

public bool IsSatisfied(int iValue) 
{
	if ( m_iMinValueOperatorID == Constants.COMPAREOPERATORID_GREATER ){
		if ( !( iValue > m_iMinValue ) ){
			return false;
		}	
	}

	if ( m_iMinValueOperatorID == Constants.COMPAREOPERATORID_GREATEROREQUAL ){
		if ( !(iValue >= m_iMinValue ) ){
			return false;
		}
	}

	if ( m_iMaxValueOperatorID == Constants.COMPAREOPERATORID_LESS ){
		if ( !( iValue<m_iMaxValue ) ){
			return false;
		}	
	}

	if ( m_iMaxValueOperatorID == Constants.COMPAREOPERATORID_LESSOREQUAL ){
		if ( !( iValue <= m_iMaxValue ) ){
			return false;
		}	
	}
	return true;
}

public void GetRange(int iMinValueOperatorID, int iMaxValueOperatorID, int iMinValue, int iMaxValue )
{
    iMinValueOperatorID = m_iMinValueOperatorID;    // Å¬’l”äŠr‰‰ŽZŽq‚h‚c.
    iMaxValueOperatorID = m_iMaxValueOperatorID;    // Å‘å’l”äŠr‰‰ŽZŽq‚h‚c.
    iMinValue = m_iMinValue;                // Å¬’l.
    iMaxValue = m_iMaxValue;				// Å‘å’l.
}


    }
}
