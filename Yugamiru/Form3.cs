﻿/*using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
    }
}*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Yugamiru
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        [DllImport("Gdi32.dll", SetLastError = true)]
        private static extern int StretchDIBits(
            IntPtr hdc,                      // handle to DC
            int XDest,                    // x-coord of destination upper-left corner
            int YDest,                    // y-coord of destination upper-left corner
            int nDestWidth,               // width of destination rectangle
            int nDestHeight,              // height of destination rectangle
            int XSrc,                     // x-coord of source upper-left corner
            int YSrc,                     // y-coord of source upper-left corner
            int nSrcWidth,                // width of source rectangle
            int nSrcHeight,               // height of source rectangle
            IntPtr lpBits,                // bitmap bits
            IntPtr lpBitsInfo,            // bitmap data
            int iUsage,                   // usage options
            int dwRop);                   // raster operation code

        [DllImport("Gdi32.dll", SetLastError = true)]
        private static extern int SetStretchBltMode(IntPtr hdc, int iStretchMode);

        // BITMAPINFO with embedded BITMAPINFOHEADER to simplify the code
        struct BITMAPINFO
        {
            public int biSize;
            public int biWidth;
            public int biHeight;
            public short biPlanes;
            public short biBitCount;
            public int biCompression;
            public int biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public int biClrUsed;
            public int biClrImportant;

            public int bmiColors;
        };

        struct RGBQUAD
        {
            public byte rgbBlue;
            public byte rgbGreen;
            public byte rgbRed;
            public byte rgbReserved;
        };

        const int COLORONCOLOR = 3;
        const int HALFTONE = 4;
        const int DIB_RGB_COLORS = 0;
        const int SRCCOPY = 0x00CC0020;

        Image testImage = Yugamiru.Properties.Resources.sokui;
         int imageWidth =  256;
         int imageHeight = 256;

        byte[] pixels;
        IntPtr pixelsPtr = (Yugamiru.Properties.Resources.sokui).GetHbitmap();
        GCHandle pixelsHandle;

        IntPtr bitmapInfoPtr;

        private void Form3_Load(object sender, EventArgs e)
        {
            imageWidth = testImage.Size.Width;
            imageHeight = testImage.Size.Height;
            pixels = new byte[imageWidth * imageHeight];   // sample image

            for (int i = 0; i < pixels.Length; i++)    // fill it by some way
            {
                pixels[i] = (byte)i;
            }

            // make unmanaged pointer to pixels (you must do this for every image)
            pixelsHandle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            pixelsPtr = pixelsHandle.AddrOfPinnedObject();

            CreateBitmapInfo();
        }

        // allocate BITMAPINFO and color palette in unmanaged memory
        private void CreateBitmapInfo()
        {
            BITMAPINFO info = new BITMAPINFO();

            info.biSize = Marshal.SizeOf(typeof(BITMAPINFO)) - Marshal.SizeOf(typeof(Int32));  // sizeof BITMAPINFOHEADER
            info.biWidth = imageWidth;
            info.biHeight = imageHeight;
            info.biPlanes = 1;
            info.biBitCount = 24;
            info.biCompression = 0;     // BI_RGB
            info.biSizeImage = imageWidth * imageHeight;
            info.biXPelsPerMeter = 0;
            info.biYPelsPerMeter = 0;
            info.biClrUsed = 0;
            info.biClrImportant = 0;
            info.bmiColors = 0;         // to prevent warning

            // allocate memory
            bitmapInfoPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(BITMAPINFO)) +
                Marshal.SizeOf(typeof(RGBQUAD)) * 1);

            // copy BITMAPINFO
            Marshal.StructureToPtr(info, bitmapInfoPtr, false);

            int offset = Marshal.SizeOf(typeof(BITMAPINFO)) - Marshal.SizeOf(typeof(Int32));

            RGBQUAD color = new RGBQUAD();

            // copy color palette
            for (int i = 0; i < 256; i++)
            {
                color.rgbRed = (byte)i;
                color.rgbGreen = (byte)i;
                color.rgbBlue = (byte)i;
                color.rgbReserved = 255;

                /*
                  IntPtr p = new IntPtr(bitmapInfoPtr.ToInt32() + offset);

                  Marshal.StructureToPtr(color, p, false);

                  offset += Marshal.SizeOf(typeof(RGBQUAD));
                  */
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Release handle to pixels. You must do this for every frame
            pixelsHandle.Free();

            Marshal.FreeHGlobal(bitmapInfoPtr);
        }

        // Draw directly from pixels array to screen without additional copying
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            IntPtr hdc = e.Graphics.GetHdc();

            int oldStretchMode = SetStretchBltMode(hdc, HALFTONE);  // high quality
            //int oldStretchMode = SetStretchBltMode(hdc, COLORONCOLOR);  // fast

            var t = StretchDIBits(
                hdc,
                0,
                0,
                pictureBox1.Width,
                pictureBox1.Height,
                0,
                0,
                imageWidth,
                imageHeight,
                pixelsPtr,
                bitmapInfoPtr,
                DIB_RGB_COLORS,
                SRCCOPY);

            SetStretchBltMode(hdc, oldStretchMode);

            e.Graphics.ReleaseHdc(hdc);
        }

      
    }
}
