﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Yugamiru
{
    public partial class Form4 : Form
    {
        PrintPreviewDialog printPreviewDialog1 = new PrintPreviewDialog();
        PrintDocument printDocument1 = new PrintDocument();
        public Form4()
        {
            InitializeComponent();
            Image test = Yugamiru.Properties.Resources.sokui;

            Image img = new Bitmap(test.Width, test.Height);
            using (Graphics gr = Graphics.FromImage(img))
            {
                gr.DrawImage(test, new Point(0, 0));
                gr.DrawImageUnscaled(Yugamiru.Properties.Resources.sokui, 0,0,100,200);
            }
            
            pictureBox1.Image = img;
            
        }

        private int _Line = 0;

        void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            /*   Font myFont = new Font("m_svoboda", 14, FontStyle.Underline, GraphicsUnit.Point);

               float lineHeight = myFont.GetHeight(e.Graphics) + 4;

               float yLineTop = e.MarginBounds.Top;*/

            /*   for (; _Line < 70; _Line++)
               {
                   if (yLineTop + lineHeight > e.MarginBounds.Bottom)
                   {
                       e.HasMorePages = true;
                       return;
                   }

                   e.Graphics.DrawString("TEST: " + _Line, myFont, Brushes.Black,
                       new PointF(e.MarginBounds.Left, yLineTop));

                   yLineTop += lineHeight;
               }*/
            
            for (; _Line < 2; _Line++)
            {
               
                    e.HasMorePages = true;
               
            }

             //e.HasMorePages = false;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;

            _Line = 0; // initialize printing
            this.printDocument1.PrintPage += printDocument1_PrintPage;
            DialogResult result =   printPreviewDialog1.ShowDialog();
            if(result == DialogResult.Cancel)
            {
                printPreviewDialog1.Dispose();
            }

        }

       
    }
}
