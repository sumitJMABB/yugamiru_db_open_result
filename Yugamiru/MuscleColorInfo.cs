﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MuscleColorInfo
    {

        List<MuscleColorInfoElement> m_listMuscleColorInfoElement  = new List<MuscleColorInfoElement>();
        // ‹Ø“÷•\Ž¦Fî•ñ.
        public MuscleColorInfo( )
{
	m_listMuscleColorInfoElement.Clear();
}

  

public int GetMuscleColor(int iMuscleID) 
{
            /*std::list<MuscleColorInfoElement>::const_iterator it = m_listMuscleColorInfoElement.begin();
            for( it = m_listMuscleColorInfoElement.begin(); it != m_listMuscleColorInfoElement.end(); it++ ){
                if ( it->GetMuscleID() == iMuscleID ){
                    return it->GetMuscleColorID();
                }	
            }*/
            for (int j = 0; j < m_listMuscleColorInfoElement.Count; j++)
            {
                if(m_listMuscleColorInfoElement[j].GetMuscleID() == iMuscleID)
                {
                    return m_listMuscleColorInfoElement[j].GetMuscleColorID();
                }
               
            }
           

    return Constants.MUSCLECOLORID_NONE;
}

public void AddData(int iMuscleID, int iMuscleColorID)
{
 /*   std::list<CMuscleColorInfoElement>::iterator it = m_listMuscleColorInfoElement.begin();
    for (it = m_listMuscleColorInfoElement.begin(); it != m_listMuscleColorInfoElement.end(); it++)
    {
        if (it->GetMuscleID() == iMuscleID)
        {
            it->SetMuscleColorID(iMuscleColorID);
        }
    }*/
    foreach (var Element in m_listMuscleColorInfoElement )
            {
                if (Element.GetMuscleID() == iMuscleID)
                    Element.SetMuscleColorID(iMuscleColorID);
            }
    MuscleColorInfoElement MuscleColorInfoElement = new MuscleColorInfoElement(iMuscleID, iMuscleColorID);
    m_listMuscleColorInfoElement.Add(MuscleColorInfoElement);
}

public void Clear( )
{
    m_listMuscleColorInfoElement.Clear();
}

    }
}
